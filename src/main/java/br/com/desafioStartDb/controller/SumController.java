package br.com.desafioStartDb.controller;

import br.com.desafioStartDb.model.Somar;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SumController {

    @GetMapping("/somar")
    public Somar somarNumeros(@RequestParam(defaultValue = "0") int a, @RequestParam(defaultValue = "0") int b){
        return new Somar(a,b);
    }
}

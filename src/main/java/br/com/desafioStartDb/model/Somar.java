package br.com.desafioStartDb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Somar {
    private Integer a;
    private Integer b;
    private Integer sum;

    public Somar(Integer a, Integer b){
        this.a =  a;
        this.b = b;
        this.sum = a + b;
    }

}
